import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WebsocketService } from './services/websocket.service';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { configuration } from './configuration/configuration';
import { CategoryComponent } from './components/category/category.component';
import { RolesComponent } from './components/roles/roles.component';
import { ProductsComponent } from './components/products/products.component';
import { PuntosComponent } from './components/puntos/puntos.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { RootComponent } from './components/root/root.component';
import { BodegaComponent } from './components/bodega/bodega.component';
import { TrasladoComponent } from './components/traslado/traslado.component';
const configSocket: SocketIoConfig = { url: configuration.bussinesServer.url , options: {} };

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotfoundComponent,
    CategoryComponent,
    RolesComponent,
    ProductsComponent,
    PuntosComponent,
    UsuariosComponent,
    RootComponent,
    BodegaComponent,
    TrasladoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SocketIoModule.forRoot(configSocket),
  ],
  providers: [ TrasladoComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
