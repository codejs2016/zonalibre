export const configuration = {
   bussinesServer:{
    url:'http://localhost:3000'
    //url:'http://144.91.67.1:3000'
   },
   initComponent:{
    login:'Entrar',
    register:'Registrar',
    zl:'Zona Libre',
    },
    loginComponent:{
     titlte:'Zona Libre',
     placeholderUser:'Usuario',
     placeholderPassword:'Contraseña',
     textLogin:'Iniciar sesión' 
    },
    resetComponent:{
      titlte:'Zona Libre',
      placeholderUser:'Usuario',
      textreset:'Recuperar contraseña' 
    },
    modulos:[
      { name:'Bodega', icon:'ion-ios-calendar', call:'bodega' },
      { name:'Categorias', icon:'ion-ios-calculator', call:'categorias' },
      { name:'Productos', icon:'ion-ios-basket', call:'productos' },
      { name:'Puntos Ventas', icon:'ion-md-wallet', call:'puntos' },
      { name:'Usuarios', icon:'ion-ios-people', call:'usuarios' },
    ],
    modulosbd:[
      { name:'Bodega', icon:'ion-ios-calendar', call:'bodega' },
      { name:'Categorias', icon:'ion-ios-calculator', call:'categorias' },
      { name:'Productos', icon:'ion-ios-basket', call:'productos' }
    ],
    typerol:{
      root:'5ca579e80f91700d0b03cd04',
      bodega:'5ca579e80f91700d0b03cd05',
      auxbodega:'5ca579e80f91700d0b03cd06',
    },
    typepresentation:[
      { id:1, name:'Caja' },
      { id:2, name:'Costales'},
      { id:3, name:'Unidades'}
    ]
  }