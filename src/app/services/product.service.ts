import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService
    ) 
   {
   }

  ngOnInit(){}

  seach(prod: any){
    return this.http.post(this.URL_API+'/api/product/search', prod)
  }

  save(data: any){
    return this.http.post(this.URL_API+'/api/product/create', data)
  }

  update(data: any){
    return this.http.post(this.URL_API+'/api/product/update', data)
  }

  uploap(data: any){
    return this.http.post(this.URL_API+'/api/product/upload', data)
  }

  delete(data: any){
    return this.http.post(this.URL_API+'/api/product/delete', data)
  }

  get(){
    return this.http.get(this.URL_API+'/api/product')
  }
}
