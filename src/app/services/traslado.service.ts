import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class TrasladoService {
  
  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService
    ) 
   {
   }

  ngOnInit(){}

  save(data: any){
    return this.http.post(this.URL_API+'/api/traslado/create', data)
  }

  update(data: any){
    return this.http.post(this.URL_API+'/api/traslado/update', data)
  }

  delete(data: any){
    return this.http.post(this.URL_API+'/api/traslado/delete', data)
  }

  get(){
    return this.http.get(this.URL_API+'/api/traslado')
  }

  getByPunto(punto:string){
    return this.http.post(this.URL_API+'/api/traslado/get', {name:punto})
  }

  getStock(id:string){
    return this.http.get(this.URL_API+`/api/traslado/stock/${id}`)
  }
}
