import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService{
  
  constructor(private socket: Socket)
  {

  }

  sendTraslado(data: any)
  {
    this.socket.emit('traslado', data)
  }
}
