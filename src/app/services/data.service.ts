import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public title: any;
  public print: any;
  public puntoventa: string;
  public counter: number;
  public dataSend = [];
  public username: string
  constructor() { }
}
