import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  
  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService
    ) 
   {
   }

  ngOnInit(){}

  get(){
    return this.http.get(this.URL_API+'/api/documentypes')
  }

  getGender(){
    return this.http.get(this.URL_API+'/api/gender')
  }

  getRole(){
    return this.http.get(this.URL_API+'/api/rols')
  }
  
}
