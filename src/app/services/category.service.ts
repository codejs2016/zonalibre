import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config-service';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  
  readonly URL_API = this.config.getConfig().bussinesServer.url
  constructor(
    private http: HttpClient, 
    private config:ConfigService
    ) 
   {
   }

  ngOnInit(){}

  save(data: any){
    return this.http.post(this.URL_API+'/api/category/create', data)
  }

  update(data: any){
    return this.http.post(this.URL_API+'/api/category/update', data)
  }

  delete(data: any){
    return this.http.post(this.URL_API+'/api/category/delete', data)
  }

  get(){
    return this.http.get(this.URL_API+'/api/category')
  }
}
