import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { RootComponent } from './components/root/root.component';
import { AuthGuard } from './guards/auth.guard';
import { BodegaComponent } from './components/bodega/bodega.component';
import { PuntosComponent } from './components/puntos/puntos.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { ProductsComponent } from './components/products/products.component';
import { CategoryComponent } from './components/category/category.component';
import { TrasladoComponent } from './components/traslado/traslado.component';
import { ChildGuard } from './guards/child.guard';

const routes: Routes = [
  { 
    path: 'user', component: RootComponent,
    // canActivate: [ AuthGuard ],
      ///     canActivateChild: [ ChildGuard ],
    children: [
    { path: 'categorias', component: CategoryComponent },
    { path: 'bodega', component: TrasladoComponent  },
    { path: 'puntos', component: PuntosComponent  },
    { path: 'productos', component: ProductsComponent },
    { path: 'usuarios', component: UsuariosComponent  }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'not-found', component: NotfoundComponent  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
