import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';

import { map, take } from  'rxjs/operators'
import { LoginService } from '../services/login.service';
import { ConfigService } from '../services/config-service';

@Injectable({
  providedIn: 'root'
})
export class ChildGuard implements  CanActivateChild {

  constructor(private loginService: LoginService, 
    private router: Router,
    private configuration: ConfigService
    ){ }
    
   canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
       return this.loginService.islog(sessionStorage.getItem('token'))
        .pipe(take(1))
        .pipe(map( (res:any) => {
         if(res.status == true){
            return true
         }
         else{
           this.router.navigate(['login'])
           return false
         }
      }))
  }
  
}
