import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilServices{

  constructor() { }

  validateStringNull(paramString : String)
  {
     return (!paramString || paramString == undefined || paramString == "")
  }

  replaceAll( text: any, busca: any, reemplaza: any )
  {
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca,reemplaza)
    return text;
  }

  resetForm(form: any) 
  {
    if (form) {
      form.reset()
    }
  }

  validateNumber(event: any) 
  {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
  }

  validateNumberAndLetters(event: any) 
  {
    event.target.value = (event.target.value + '').replace(/[^0-9a-zA-Z0-9]*$/g, '')
  }
  
  formatNumber(num: any, prefix: any)
  {
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + '.' + '$2');
    }
    return prefix + splitLeft + splitRight;
  }

  toast(mode:any, message:any)
  {
    
  }
}
