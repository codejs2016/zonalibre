import { Component, OnInit } from '@angular/core';
import { DocumentService } from 'src/app/services/documents.service';
import swal from 'sweetalert2';
import { ConfigService } from 'src/app/services/config-service';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UtilServices } from '../util/util';
import { DataService } from 'src/app/services/data.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  public formR: FormGroup
  public fullName: AbstractControl
  public documentType: AbstractControl
  public documentNumber: AbstractControl
  public password: AbstractControl
  public phone: AbstractControl
  public gender: AbstractControl
  public userName: AbstractControl
  public role: AbstractControl

  public showLoader = false
  public submitted = false
  public showError = false
  public hidepass = false
  public sb = true
  public sb2 = false
  public userList: any
  public msgError: string

  public showform = false
  public typeGender: any
  public typeDoc: any
  public typeRol: any

  constructor(
    private formBuilder: FormBuilder,
    private config: ConfigService,
    private documentService: DocumentService,
    private userService: UserService,
    public utilService: UtilServices,
    private dataService: DataService) 
    {
    this.formR = this.formBuilder.group({
      _id: [''],
      documentNumber: ['', Validators.required ],
      fullName: ['', Validators.required ],
      password: ['', Validators.required ],
      phone: ['', Validators.required ],
      documentType: ['Cedula', Validators.required ],
      gender: ['Femenino', Validators.required ],
      userName: ['', Validators.required ],
      role: ['5ca579e80f91700d0b03cd05', Validators.required ]
    })
    this.documentNumber = this.formR.controls['documentNumber']
    this.fullName = this.formR.controls['fullName']
    this.password = this.formR.controls['password']
    this.phone = this.formR.controls['phone']
    this.documentType = this.formR.controls['documentType']
    this.gender = this.formR.controls['gender']
    this.userName = this.formR.controls['userName']
    this.role = this.formR.controls['role']
  }

  ngOnInit() {
    this.getUsers()
    this.getGenders()
    this.getDocuments()
    this.getRols()
    this.dataService.title = "Gestión de Usuarios"
  }


  get f() {
    return this.formR.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  showHide(){
    this.showform = !this.showform
    
  }

  clear(){
    this.formR.reset()
  }

  getUsers(){
    this.show()
    this.userService.get().subscribe( (res:any) =>{
      this.userList = res
      this.sb = true
      this.sb2 = false
      this.formR.reset()
    }, error => {
      swal.fire("Aviso","Ocurrio un error al consultar usuarios", "warning")
      this.hide()
    }) 
    this.hide()
  }

  saveEdit(){
    this.submitted = true
    if(this.formR.invalid)
     return false

    if(this.formR.value._id != ""){
      this.show()
      this.userService.update(this.formR.value).subscribe( (res:any)=>{
        if(res.status == true)
         {
          this.getUsers()
          this.showHide()
          this.formR.reset()
          this.submitted = false
         }
      }, error =>{
         this.hide()
         swal.fire("Aviso",this.config.getMessage().error.message + "al actualizar usuario", "warning")
      })
    }
  }

  save(){
    
    this.submitted = true
    if(this.formR.invalid)
    return false

    this.show()
    this.userService.save(this.formR.value).subscribe( (res:any)=>{
      if(res.status == true)
      {
        this.getUsers()
        this.showHide()
        this.formR.reset()
        this.submitted = false
        
      }
    }, error =>{
      this.hide()
      swal.fire("Aviso",this.config.getMessage().error.message + "al registrar usuario", "warning")
    })
  }
  
  delete(id: string){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Está seguro eliminar usuario ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      
       for (let i = 0; i < this.userList.length; i++) {
         if(id == this.userList[i]._id){
           this.userList.splice(i,1)
           this.userService.delete({ _id: id }).subscribe( (res:any)=>{
            if(res.status == true)
               this.getUsers()
          }, error =>{
             swal.fire("Aviso",this.config.getMessage().error.message + "al eliminar punto de venta", "warning")
          })
         }
       }
     }
    })
  }

  edit(item: any){
    console.log( item );
    this.hidepass = false
    this.sb = false
    this.sb2 = true
    this.showHide()
    this.formR.get('_id').setValue(item._id)
    this.formR.get('documentNumber').setValue(item.documentNumber)
    this.formR.get('fullName').setValue(item.fullName)
    this.formR.get('password').setValue(item.password)
    this.formR.get('phone').setValue(item.phone)
    this.formR.get('documentType').setValue(item.documentType)
    this.formR.get('gender').setValue(item.gender)
    this.formR.get('userName').setValue(item.userName)
    this.formR.get('role').setValue(item.role)
  }

  validateNumberFormat(event: any, opc?:number) 
  {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    //if(opc > 0)
      //this.formR.get('documentNumber').setValue(this.utilService.formatNumber(event.target.value, " "))
  }

  getDocuments(){
    this.documentService.get().subscribe( (res:any)=>{
     this.typeDoc = res
    }, error =>{
      swal.fire("",this.config.getMessage().error.message + "al registrar consultar tipo de documentos", "warning")
    })
  }

  getGenders(){
    this.documentService.getGender().subscribe( (res:any)=>{
     this.typeGender = res
    }, error =>{
      swal.fire("",this.config.getMessage().error.message + "al registrar consultar tipos de generos", "warning")
    })
  }

  getRols(){
    this.documentService.getRole().subscribe( (res:any)=>{
     this.typeRol = res
    }, error =>{
      swal.fire("",this.config.getMessage().error.message + "al registrar consultar tipos de roles", "warning")
    })
  }
  

  

}
