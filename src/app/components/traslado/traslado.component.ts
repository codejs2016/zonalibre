import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/services/config-service';
import { PuntoService } from 'src/app/services/punto.service';
import { DataService } from 'src/app/services/data.service';
import swal from 'sweetalert2';
import { ProductService } from 'src/app/services/product.service';
import { UtilServices } from '../util/util';
import { CategoryService } from 'src/app/services/category.service';
import { TrasladoService } from 'src/app/services/traslado.service';
import { WebsocketService } from 'src/app/services/websocket.service';

declare let pdfMake: any ;

@Component({
  selector: 'app-traslado',
  templateUrl: './traslado.component.html',
  styleUrls: ['./traslado.component.css']
})
export class TrasladoComponent implements OnInit {

  public formR: FormGroup
  public puntoventa: AbstractControl
  public producto:  AbstractControl
  public referencia: AbstractControl
  public ctg: AbstractControl
  public cantidad: AbstractControl
  public vtotal: AbstractControl

  public showLoader = false
  public submitted = false
  public showError = false
  public trasladosData: any
  public msgError: string

  public showform = false
  public listCtg: any
  public listPtv: any
  public dataSend = []
  public idPuntoVenta: any
  
  constructor(
    private formBuilder: FormBuilder,
    private config: ConfigService,
    private productService: ProductService,
    private categoryService: CategoryService,
    private trasladoService: TrasladoService,
    private puntosService: PuntoService,
    public utilService: UtilServices,
    private dataService: DataService,
    private webSocketService: WebsocketService) 
    {
    this.formR = this.formBuilder.group({
      _id: [''],
      puntoventa: ['5e24070cd64309342e5dd2d7', Validators.required ],
      producto: ['', Validators.required ],
      referencia: ['', Validators.required ],
      ctg: ['', Validators.required ],
      cantidad: ['', Validators.required ],
      vtotal: ['', Validators.required ],
    })
    this.puntoventa = this.formR.controls['puntoventa']
    this.producto = this.formR.controls['producto']
    this.referencia = this.formR.controls['referencia']
    this.ctg = this.formR.controls['ctg']
    this.cantidad = this.formR.controls['cantidad']
    this.vtotal = this.formR.controls['vtotal']
  }

  ngOnInit() {
    this.getCTg()
    this.getPtvs()
    this.dataService.title = "Gestión de Traslados"
    this.dataService.print = "<i class='ion-ios-print'></i>"
  }
 
  getCTg(){
    this.categoryService.get().subscribe( (res:any)=>{
      this.listCtg =  res
    }, error =>{
      swal.fire("Aviso","Ocurrio un error al consultar categorias", "warning")
    })
  }

  getPtvs(){
    this.puntosService.get().subscribe( (res:any)=>{
      this.listPtv = res
    }, error =>{
      swal.fire("Aviso","Ocurrio un error al consultar puntos de ventas", "warning")
    })
  }
  get f() {
    return this.formR.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  showHide(){
    this.showform = !this.showform
  }

  clear(){
    this.formR.reset()
    this.dataService.dataSend = []
    this.dataService.puntoventa = null
  }

  getTraslados(){
    this.show()
    this.trasladoService.get().subscribe( (res:any) =>{
      this.trasladosData = res
    }, error => {
      swal.fire("Aviso","Ocurrio un error al consultar traslados", "warning")
      this.hide()
    }) 
    this.hide()
  }

  edit(item: any){
    this.showHide()
    this.formR.get('_id').setValue(item._id)
    this.formR.get('name').setValue(item.name)
    this.formR.get('ctg').setValue(item.ctg)
    this.formR.get('vunit').setValue(item.vunit)
  }

  validateNumberFormat(event: any) 
  {
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    this.formR.get('vunit').setValue(this.utilService.formatNumber(event.target.value, " "))
  }
   
  searchProduct(event:any){
    this.showLoader = true
    this.productService.seach({ name: event.target.value }).subscribe( (res:any)=>{
       if(res.status == true){
         if(res.product.length >0 )
         {
          for(let i = 0; i < res.product.length; i++) {
            res.product[i].referencia = ''
            res.product[i].cnt = ''
            this.showLoader = false
          }
          this.trasladosData = res.product
         }
         else
          this.showLoader = false
       }else
        this.showLoader = false
        
    }, error =>{
      this.msgError = "Ocurrio un error al consultar producto"
      this.showError = true
      this.showLoader = false
    })
   }

   changeCantidad(event:any,id:any){
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    for(let i = 0; i < this.trasladosData.length; i++) {
      if(this.trasladosData[i]._id ==  id){
       this.trasladosData[i].cnt = event.target.value
      }
    }
  }

  delete(id:any) {
    for(let i = 0; i < this.trasladosData.length; i++) {
       if(this.trasladosData[i]._id ==  id){
         this.trasladosData.splice(i,1)
       }
    }
  }

  clearTraslado(id:any) {
    for(let i = 0; i < this.trasladosData.length; i++) {
       if(this.trasladosData[i]._id ==  id){
        this.trasladosData[i].cnt = ""
       }
    }
  }
  
  add(item:any){
    this.validarStock(item)
  }

  validarStock(item:any){
   this.trasladoService.getStock(item._id).subscribe( (res:any)=>{
     if(res._id == item._id){
      if(parseInt(item.cnt) > 0){
       if( res.stock > parseInt(item.cnt) ){
        this.dataService.dataSend.push(item)
        this.delete(item._id)
        this.dataService.print = 1
        this.dataService.counter = this.dataSend.length
       }else{
        swal.fire('No hay disponibilidad del producto en bodega') 
       }
     }else
      swal.fire('Por favor agregar la cantidad a trasladar') 
     }
   },
   error =>{
    swal.fire('Ocurrio un error al validar stock de productos') 
   })
  }

  save(){
    if(this.dataService.puntoventa != null){
      if(this.dataService.dataSend.length > 0){
        const swalWithBootstrapButtons = swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })
    
        swalWithBootstrapButtons.fire({
          title: 'Está seguro de registrar traslado?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
          reverseButtons: true
        }).then((result) => {
        if (result.value)
        {
            this.show()
            let data = {
              data:this.dataService.dataSend,
              idpunto: this.dataService.puntoventa
            }
            this.trasladoService.save(data).subscribe( (res:any)=>{
            if(res.status == true)
            {
              this.hide()
              this.generatePdf()
              this.socketSend()
              setTimeout(() => {
                this.clearDataService()
                this.dataService.print = 0
              }, 1000);
            }
          }, error =>{
            this.clearDataService()
            this.hide()
            swal.fire("Aviso",this.config.getMessage().error.message + "al registrar traslado", "warning")
          })
         }
        })
      }
    }
    else
      swal.fire('Por favor seleccionar Punto de Venta')
  }

  clearDataService(){
    this.dataService.dataSend = []
    this.dataService.puntoventa = null
  }

  socketSend(){
    let data = {
      traslado:this.dataService.dataSend,
      punto:this.dataService.puntoventa
    }
    this.webSocketService.sendTraslado(data)
  }

  getPoint(punto:any){
    this.dataService.puntoventa = punto
  }

  generatePdf()
  {
    let dataBody = []
    let dataBody2 = []
    let dataBody3 = []
    let dataBody4 = []
    let dataBody5 = []
    let suma = 0
   
    for (let i = 0; i < this.dataService.dataSend.length; i++) {
      this.dataService.dataSend[i].ref = this.dataService.dataSend[i].ref.toUpperCase()
      this.dataService.dataSend[i].name= this.dataService.dataSend[i].name.toUpperCase()
      this.dataService.dataSend[i].ctg = this.dataService.dataSend[i].ctg.toUpperCase()
      this.dataService.dataSend[i].vventa =
       this.utilService.formatNumber(this.dataService.dataSend[i].vventa, " ")
      dataBody.push(this.dataService.dataSend[i].ref)
      dataBody2.push(this.dataService.dataSend[i].name)
      dataBody3.push(this.dataService.dataSend[i].ctg)
      dataBody4.push(this.dataService.dataSend[i].vventa)
      
      dataBody5.push(this.dataService.dataSend[i].cnt)
      suma += this.dataService.dataSend[i].cnt << 0
    }
    
    let fecha = new Date()
    let dateNow = fecha.getDay()+"/"+fecha.getMonth()+"/"+fecha.getFullYear()
    var contentPdf = {
	  content: [
      {
        image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISDxUQDxAQFQ8QDxUPDw8PDxAPDw8PFRUWFhURFRUYHSggGBolGxUVIjEiJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGislHyUuLS0vKystLS0tLSstNy01LSswLSstKy03Ky03LS0uLS0rLi0rLi0tLS0tKy0tLS0tL//AABEIAKYBMAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEDBAcGBQj/xABOEAABAwIDAwgGBAoGCgMAAAABAAIDBBEFEiEGEzEHIkFRYXGBkRQykpOh0VJTYsEVM0NygqKxssLSCCNClNPhFlRjc3SDo7Pw8SQlRP/EABoBAQEBAQEBAQAAAAAAAAAAAAABBAMCBQb/xAAvEQACAQMACAUDBQEAAAAAAAAAAQIDBBETFSExQVGBoQUSFFLRQnHwIzKRsfEi/9oADAMBAAIRAxEAPwDDrJKQt4+CEBANZOAiDUYagBDUQajDUYahCMNUgYjDVIGICIMRNjUwYjDEBG2NFu1K1iMMQEG7TiJThikDUBWESW7VksTZEBXyJbtWciWRAVt2lu1ayJZEBU3aYxq2WISxAVCxCWK2WICxAVCxCWK0WISxAVCxCWq0WIHNQFYtQlqsFqAtQEBahIUxahIQERCEhTEIS1ARWSKMhA5CjJJJIC3I3Q/nAfAlRhqszDm98h+AAUQCAYNRtanaFIAhBg1SBqTVKAgGDUYanaFI0IUZrFK2JExqnYhAGwICyyttKF4ugK4ansjAT2QAWTgI7JWQAWT5USSAHKmsjTEIUCyYhepguE+kvdGJ4IpA0GNtQ8xtmcTbIH2sD38dLdNqVZTOikdG+2eNxY8Nc14DgbEBzSQfBCFYhCQpChIQpCQgIUxCAhCELggIUzgoyEBCQgIUxCBwQERCAhTEICEBEQhIUpCEhAREKORTEKKVARpJJIU9OoHMb2ukP61vuUQCs1Q5sQ/2Wb2nOKhAQDAKRoTAKRoQDtCkATNCkaEA7QpGBCAjaEBK0KVqiajCAmamcgBTkoBEJk4SQgkkTInuIEcUkjnlwayKN0j3ZQC6zW6mwIKnGGVf+oV/9zqP5UBXslZW24PWHhh9f/c6j+VGMBrjww6u/uk4/hQp55TL0ajAa5jc78PrGsGpcaaawHWeboFUpKKeZjpIKeSSNhs50YzWNr2AGp0I4dYRtLeEm9xCUKZr7343BsQdCCNCCEigEUJQhxsHFrgx5cGPIs15ZbNbuzDzTkoBigKclMUABCAhGShKoIyEBCkKEoQjIQEKUoSEBEQhIUhCEhAREKCccFaIVap6EBCkkkoU9rEG2cwfRgjafZv96gCtYt+PeOrK3yY0KsEATQpAELUbUAbQjAXVbL4PHLE174w67nXJvqA4i3wXVN2Up3erEwdlr/tWaVzFNrD2GqNrJpPK2mXAIwFqX+hkY/JR+yEx2WhH5CP2QuTvorfFnRWMn9S7/BmARXWkO2eg+oj9gIDgMH1MfsBc34nSXBnReGVOa/OhnacFaD+AoPqY/ZCRwOH6qP2QprSlyf51Lqup7l3+DPrpwV6m1kTIpo2MY1odGScotch1gvILlvpVFUgpriYKtN05uD4HccjJZ+F3ZzZ/oUgiH0nZoy63blzeF1vLSF8jvmcxwkjc5r2m7XscWPaRwLXDUHtC6XB9rsVnlbCysrnPfoxkOWSVx7jbTrcTYcSupyPpYBEuU2S2eqYg2aurKuSW19w6ozws/OytGY9mo7+K6eeZrGF8jmtYwFz3vcGta0cSSdAFASLhKXDoojKIQAySpln5vqkveTcdlrLyNoOUJtZJ6Bh8ksYml9H9NEROfhvGxHMDGQHDn2JANwBo5dKN2wZW2s0WDRwAHAL5/iDzFRNtksScjMOULAs1dC6Fp3tTDOXtYwufK+Bge3K0es9wOXwC8PC9jK2olbC6kq4RIcomfSv3bD9KTMW2b1m5PUCu1xmOKsrpRIRHHhtJvfShLIx8VS9zHRgFpAaLNLi6xIyX4A30nZCplloYZam4lkYHc9jY5HNPqOewEgPLbEgeTfVGq2TVKOThcP8AUeDLeU3YuZvoMGHUs0kNLTSRkxszc5z2kueR/acczj2krN8Sw+enkEdVC6KRzcwY8tzZesgG4Hetq5YNs56JrIKV8cb5onSOlcHPlDQcuWIZS0H7Tj3DpGAuqHPkL3uc57iXOe9xe97jxc5x1J7Su5yLRTKzgzGvqI43i7HuIcLkcGOI1GvEBdq3Z6m6If15f5llr3cKLSlk1ULSdZNxwcAUJWhjZ2n+qb5uP3p/9H4PqWeSz60pcn+dTRqurzXf4M5IQ2WkfgCD6mP2Qm/AMH1MXsNTWdLkxqupzXf4M3IQkLSDgUH1MXu2fJRPwSH6mL3TPkrrKnyY1ZU5ruZ0QhIXez4TEBpFH7tnyXBjh4LVQuI1k8GW4tpUcJveRkKrVcQrhVSr4hdzMV0kklCntYnIDUS6/lXDp6CR9yga8dfwKkq/xsh65ZD+uULQgHEres+yUYmb1n2SmAThAaZsbXxNpIo3OaDZzruc1vrPc7W/eukZiEQP46L3rLftWGysBHAKvuB1DyWWVspNts1xuvKkkj6HixmBvGpg9/H80b9oKXpqaf30Z+9fOu57ktz3KelXMvqnyPoB+0VF0zQ+9j+aB20ND9dB4zR/NYHuu5Puu5ePRRfHsj3618u5s9RjdNfSeC3++j+aqSY5T/6zB76P5rI9ylulx1XDO9nZeJyX0o6TbGubJUMML2PAhsSx4e0EucbXB7l5TZnW1aLdYIVenisu85LNloa+pldU86GkEZ3F7CZ8me2fpyDdm46bjouD9ClTVOCiuB86tU0k3N8SjshsZVYk4Oibu6UEh9VIDk00LY2/lHd2gtqR07zsnspS4fHkpmc9wAlnfYzS2+k7oH2RYDqXr0sTWsDWtDWtAa1rQGta0cAANAFI6UBdDkeXtRtPBQxbycuLjfdwxNzzSkdDR0D7RsB1rG6/a9uIVGbGDVRYcxwLKOljO7Jvo6aQkE26wL/Ry9O3SVR6NFRnkLgQ4kg8QTcW6rJkGbOraerxRj6EMFDh1G2OnyRmNomnu5zmggEHKS031u0r33yWC8XCaaGGesZAGhnpxAa3QMO6hLmDqAc51h0XsvUdMF+Z8RquVw1wWw/QWNPFFPmZpDW1W+e+Sjq5KeWp9KlpxTzMFQ5l9xFK4NJ3bb8Nb3PWCLOJ4pW1jnTV0WKmZnOomUlNJFT00o1EliL+I532uAHfie3AqQVzvpFa14ukseTuZ34Zl583YyvbXFq6tZBJWUs7X00DoZJ3U8sbJRmzCR12gNNuPRfUWvYcowHsW91E+Yam9+jjfsssOrmNE8rY7bsTyCMDhuw8htuy1lus7z1GdmMGO6tdBjbnJZ2feRVwkkWznoA4tcPvWktqm9Y8wsmljuFWMHYPJW6s1XabeMHq1vHQi1jOTZPTWfSb5hMa5n0m+0Fjm57B5JbjsHks2qo+5ml+KP29zYDiUf02e0EBxSL6yP22/NZFue7yTbnu8ldVQ9zJrSXtRrTsWh+ti94z5qCTGIR+Wh96z5rLd13eScRL0vDIe5k1nP2o0WbGID+Xh96z5rgHcTbhc27lG1lkQWy3tlRzh7zJcXLrYytw5VOs4hXFUrOIWhmYrJJJLyD1Zzz3Hre4/EpNQE3160TSgJAiQBGEAxQEKWyINQEGVKysBqcNUKVrJw3sVrKEg1UFbKkGlWsoSyhARMV/A8fqKKff0smV5GV4cM0cjOOV7ekX8R0EKoQonsQh3o5Z8R4buh79xUf4ygfyw4if7NGO6Cb75VwZjCbdhAdw7lbxE9FJ7iT/ABFE7lVxE9NMO6A/e5cZuwluwrsB6FHtBURSPljlOaV5klu1rmyPcS4uLSLXu46ix1XpHbmq64x/yvmVz27CYxhcp0ac3mUU+h1jVnFYjJo9923FX9KP3TUB22q/rGeELPkvC3QT7oLz6aj7F/CL6ir7n/J7D9sKx2m+IHTliiB88ui8mPs4dA6k27CkY1dIUow/akvseJVJS/c8hqMtUwCWVdDwV8qbKrJamyJgFbKllVgtTZUBXslZTFqWVAQ2SUtkJCAAqpWcQrhVOt4hGCskkkvIPQCIFRgogUBKCjBUIKNpQEwKLMogUnFAejQ4TUTM3kTWlpJAJc1tyNDxVqLZms6WM9tigotp5IY2xsjiytva+e5JJJJsesldxs/UzSRbydjGZtWMaHA5fpOuenq/8Hzbi4r0sywscOZ9Ohb0KuFl54nFVuDzwszytaGggeu03J4CwQ0eFzz6wR2ZwL3usy/UCdT4A2XuY5VCrqo6SP8AFtkvI8dJAOa3c3ML9ZXv4vWNpaUva0c0BkTOjMdGjuHHuBUd3UUYrH/UuHIRtKTlJ5flj3OVGx1Va4kgv1Z5P5F5FbDJC50U0ZbK0Zm2Nw4dB00I0Oo6iO7r9isWnndLvnBzWBpacrW2cS7TTjoFBt+3NLAxtt44OaP0nMDfjdeqVzV02iqY6Hmtb0tDpKeep4kOzlVLG2SPJleA5pc8C7TwPBeKZuhwIc0lrrDMdOy/HQ9n37LFTNZG1jeDGhg7gLD9izHEcJJxKSK3NfVAkH6ElpD5NefJW1vdLKSfDb0Jc2ipxj5d72dQRs1U5N64R7sMMhBkyuDAM2vNsDZeNvL8Ab30HE9g7VpG2FRu6KTrktEP0jZ36uZctsLhm+qg9wvHT2kPUZL8weYLv0QvVC7cqMqs9yJcWsY1VThvJotiK0i+SLuMtj3cF4ldSuikdFJYSRuyvDTcA9h6eK3Gke05gOLHBruxxaHW8nA+KxzayQGvqBYZhUPGYaXA6/gvFld1K0mppI8XVvCkk4nlWTtHXw4nuRtCT9Gnu/yP7V9IxnSbK8n1diEBqKb0cRCR0Q30jmOc5oFyLMOmtu8FeDjeHSUlTJSzlhlhcGvMbi5mYtDtCQL6OA4cQV9K8mOH+j4NSMIsXQCoffQh0xMpv3Z7eC+atoK30irnqL339RLKD9lzyWjwBA8FUQ9HZTZupxGV0NGIg6KPePMzsrbXDfWynUk8LdBU+1mydVhm79LNPeYPMYieZNGZcxILBb1h1rSP6PGHWgqqmxu+ZlOD2Rszm3jKPJeXyuXq8cp6IatAp6cgdDp5bvPsFh8EKeVTckmKSRska6jAexrw10rw4BwBs4brQ69aKTkexUAnNRnsE7rnsF4wFs+3WLmjw2oqYyGyRxZYSQCBK9wZHoePOcNFynJHtnV176iOr3bhCyN7ZGRiNwLy8ZXW0Pq6aDgeKhDD8RpJqaV0FTEWTxmz2SAadR00cO0EgrqsI5K8SqqeOqjfSCOeNs0bZJZGPDHC7bgREDTtXRf0gaYGqozG0GeSGWM29ZwD490D+k+S3eVrNQ9tFh7iLBlHRkjqywxafuqgw53I3io1z0R7BPJf4xLkccwaqoZN1WU5ZIQXRlxa6N7Rxc1zbh3R06XFwFqnJbt5X1mICmqnxvjNO+VxELY3NLS0AgttpdwFj1r1uXvd/gprngbxtUww/SvkfnA7Mod5BAZrgPJhiFZTR1cUlI2OYFzGyySMflDi25a2IgXtca8CFddyN4mOM1B76f8AwVtuztIKahp4DwgpYo3E9bWDMT43KwCo5V8Uc4llQ1rHOJY30enJawklrblvQLDwTaDydqdkanDt36TJTu3xcGNhfI48wDMTmY3TnN814hV3H9o6qucx1XKHmLNu7Mjjy5rZtGAXvlHkqF0AJVSs4hWiVUrOIRgrpJJLyUtgowVECiBQEoKMFQgowUBKCmLkN1YwqhNRLbURN1e7s6h2n5rzKSiss9Ri5PCPU2WwffP30g/qmHmtPCR4/hHxOnWvd2px/dN3MR/rXjnOHGNh/iPw49SixXFG00IawDMRliZ0C3SewLhZZC5xc4kucbuceJPWsFODrz0k/wBq3I+hUmreGjhve9nZbAQXlfLbRjBG3qu43PkGj2lNyhVlzFCOi8zh2+qw/wDcVzY6Hd0jD0yEynx9X9UNXJbRTumrXhvHOIGd45tvaJ81zprSXblwj/nydKn6dpGPGX+/B3Gw1NkpA4+tM4yHu9VvwaD4ry2u9IxrrZTjv/Fj9okf8F0zHsgg6o4YvJjG/ILmuT5hcZ6l/rSSZfE89/xcPJZYVG9LX6Lr+I0ThjRUer6HZPrQJmw9L4nyD9BzB/H8F5L6C+JiW3NFNnzf7W5jA9lePVYl/wDcxgO5rG7gjou9jnftLPJdmOvst4LhJO3Sa+qP9nWOKzaf0y/o4flIqR/VRfnTO8Oa0/F/kug2WoW0dFml5pymonJ/sm18vg0AeB614UtL6VjDi4Xipgy/USzUN94X+wQuyrKSOaIwyk5H2zAOLCQDe1xrbRdbiahSp0Ps5YOFOLnUnW6Ii5P53S0jp3+tPUyyns52UNv2BoHgst2kdevqf+Kl49jyFtOCUMVPC2GAWjbctGYu9Ylx1PHUlYjjrr1tR/xc5/6rlssJKdWcluMV0nGEUyESDtPcFNT05nkjgbo6eZkLOnnPIYPi8KBq6/kno99jVM0gFsRfUOB+wxxaR+mY19YwG9bY1gpMJqZGabmkeyLseW5Ix7RavlN44Ds0X0Fy815jwtkI/wD01TGO/MjDpb+0xnmvn0tcdGi7jo0dJJ4DzVRD6Y5HqDc4LT3HOmD6g9oke4s/UyLgNm//AJm1sk3FsNRUSntZAz0dh7r7srXmBlDh44COio/AMhi+TVlP9H2kc6aqqX6ubHHFnPEvkJfJ+4w+KgNW2jwGGugNPUh5iL2vIY8sJLTdtyOi+vgqVHg0OGUcgw6kc9wBk3LZLy1EgGgMjz/66B0LOeXbHZI6mnghnliMcD5pNzK+K+8cGtzFpHDdO81pOwsk7sMpnVZcah0DXPMl94Qblue+ubLlvfW904Aw3D8SnxTHqZ9Uec6qjtG1pa2KKEmUxAHX+w699bk9w+hMYw5lTTyU82bdTMMcga4tcWHiAehZNslSMftbVyRhu7gM8l22sJnFkb/HO+b4r1OXfFpIqSnjhlkjfLUueXRSOjcWRxOBF2m9s0jNOwK7wdXgGydBhgkmgYWExkyzyyPkcIm84i7jZrdLmw1sL3sFju121JxfFaeGIO9DbUxwQtcCDIJJGiSZzei7RoDqAOgkhbbQ1AqaKNxN21NIwk9YkjF/3l8+8l9ETjFO13GAySPHbHG5v7xaiB9EYk9hjeJHBsbmFj3FwYGtcMp5x4cVkO1mx2DU9DPNA4GaOFxhArTJ/WHRnNza6kaLoeWSuy4U9l9Zpoox2gO3h+Ea+fnDsHkogG0IroGlPdUDkqrV8QpyVWqeIUBCkkkoUmBRAoRIezyClDj2ey1AMCjBSa4/Z9hnyRtP5vu2H7kBFI7Rdbgz444mtzMBtmdzm6vPH5eC5gydjD/yo/kmfJpwZ7qMfcuVWlpFhs60aujeUjsJ4qaR2aTduda13P6OrivLx6GmZEN02PeOcAC11y0DUnj2W8VzCe65wt3Fp+Z/Y6zufMmvKvuaFgOLxGCNhe1r2Rhha5wbq0WuL8Romp6SkjndUGVmcuLxnlZlY48SB16njfiuCjlI/wA1ZjrbAB0Ub7XtmMwAuegMeB8Fzdmstxk1nedFevCUop43HUbT4+2WMwwu5hsZZOAcL+o3r1tcr09ma2CKkYwzwh5Be8GVgcC4k2IvxAIHgs9Lye7qHAdimjq3NAs1gsOIacx7Sb3+7sVlZwdNU1sR5jeTVR1Hte4uuq3Gr32pcagSNHT692t7NAAtIbtBTAa1EGnQJmZj2WvxWSucSbp85t0dXDVWvaQrYzwJRu50s44mibMYhTxxF8tRA2ad5mlBlYC0uJIYbnS1/MlcljVa2qrHveTu77uMhocWsbo3Q24nXszHqsvHN0QJ4jo4di9U7aMKjqZ2s81LlzgoY2I2DZrHKSKkijNTC3JE1pbJKwPabeq77XG/cVleIyh1RK8G4dPK5pHAtc9xBHmq29da2mvZ1X+ZSjCtG3jSlKSe88VKzmknwJ2le9yfbQsoMTjqZQ7c2dFKWi7mseLZgOmxDTbqBtqufCB7VpRxPpmv2ywKpjDampoZmXDxHUMDw11uOV7dDYnovquR2lxLAS+kZRDDmE4hDJUzwwRxmGmiJkdd4aLZnNY3xKxHVK5VwD6G5Rdu6GTCqmKlrIZZpoxC2ON93Fr3Na8+DC4rxuRzaKgo6F7amrijnmqXSFjyQ4MDWMb0fZJ8ViNyizlMEPpmr2mwJ8onlnw507QA2aRkbpWhvABxbcWufNeHtZywUscTmYcTNUEFrZSxzIIj9M5wC8jiABY24rAnOJN0JTANQ5H8epaaWqmrKpkckoja0zOOZ+r3SPv03LhdV+WTaSCrqaf0aZksUMDyXxnM0SSOF299o2+YWbZihJKoN92I22omYZTRzVcDJYoBE6N8ga9uQlouO4A+K43ZDE6SnxqsnfUxNhJm3EheMjxNMH809NgFmd0i5QG/YptNhU7QyeoopWB2YNlLJGh1iLgO6bE+a57FK7BRFIY2YcXiNxY1kMJc5wabAacbrILp7qAePgiJQApXQDkqCdSEqKVQEaSSSFCUwKgCkBQEue3FE2UddutV3lChMF7L4KOfQd6jzlNK7gqBJ7qO6IFQpICnuowU4KAlBTgqMFPdAS3ThRgpwUBIldCCldAFdE0qO6e6AlzJiUF010AV0robprr1kgd010GZNdMgO6RKjzJiUyA7psyAlNdMgMlDdDdK6gCumJQ3TEoAiU10BKYlQoRKjenJQlAMkkkgEnSSQD3T3SSQgTXJWJKSSFGcLGxTApJIB7ogUkkA9090kkAQKK6SSAcFPdJJAK6V0kkArpZkkkA2ZMSnSQDXTXSSVINdJMkgGuldMkgESmukkgGumSSUKNdMSkkgGTJJIBJJJID/2Q==',
        width: 100,
        margin: [200, 0, 0, 0, 0, 0]
      },
      {text: 'Buenventura - Valle del Cauca '+dateNow+' ', margin: [0, 20, 0, 8]},
      {text: 'REPORTE DE TRASLADO DE BODEGA A PUNTO DE VENTA', fontSize: 14, bold: true, margin: [80, 0, 0, 0]},
      {text: 'Origen: Bodega Principal', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
      {text: 'Destino: '+this.dataService.puntoventa.toUpperCase()+' ', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
      {
			style: 'tableExample',
			table: {
        headerRows: 1,
        widths: [150, '*', '*', '*', '*'],
				body: [
          [{text: 'REFERENCIA', style: 'tableHeader'}, 
          {text: 'PRODUCTO', style: 'tableHeader'}, 
          {text: 'SECCIÓN', style: 'tableHeader'},
          {text: '$ P.VENTA', style: 'tableHeader'},
          {text: 'CANTIDAD', style: 'tableHeader'}],
          [ dataBody, dataBody2, dataBody3 , dataBody4, dataBody5 ]
        ],
      },
    },
    { text:'TOTAL PRODUCTOS: '+this.dataService.dataSend.length+' ', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
    { text:'TOTAL CANTIDAD: '+suma+' ', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
    {text: 'Elaborado por: ', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
		{text: 'Recibe: _________________________________________________________________________', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
		{text: 'Novedad: _______________________________________________________________________', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
		{text: '_________________________________________________________________________________', fontSize: 14, bold: true, margin: [0, 5, 0, 1]},
  ],
	styles: {
		header: {
			fontSize: 18,
			bold: true,
			margin: [6, 0, 0, 0]
		},
		subheader: {
			fontSize: 16,
			bold: true,
			margin: [0, 10, 0, 5]
		},
		tableExample: {
      margin: [0, 5, 0, 15],
      uppercase: true
		},
		tableHeader: {
			bold: true,
			fontSize: 13,
			color: 'black'
		}
	},
	defaultStyle: {
		 alignment: 'justify'
	  }
	 } 
   pdfMake.createPdf(contentPdf).open();
  }
}
