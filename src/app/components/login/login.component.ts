import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config-service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public dateNow = new Date()

  public formL: FormGroup
  public user: AbstractControl 
  public contrasena : AbstractControl 
  public showLoader = false
  public submitted = false

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private config: ConfigService,
    private router: Router) 
    {
    this.formL = this.formBuilder.group({
      user: ['', Validators.required ],
      contrasena: ['', Validators.required ],
    })

    this.user = this.formL.controls['user']
    this.contrasena = this.formL.controls['contrasena']
  }

  ngOnInit() {
  }


  get f() {
    return this.formL.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  login(){
    this.submitted = true
    if(this.formL.invalid)
      return false

      this.loginService.loginUser(this.formL.value).subscribe( (res:any)=>{
        if(res.status == true){
          switch(res.user.role)
          {
              case this.config.getConfig().typerol.root:
                sessionStorage.setItem('token',res.token);
                sessionStorage.setItem('role',res.user.role);
                sessionStorage.setItem('username',res.user.fullName);
                this.router.navigate(["/user"])
              break;
  
              case this.config.getConfig().typerol.bodega:
              case this.config.getConfig().typerol.auxbodega:
                sessionStorage.setItem('token',res.token);
                sessionStorage.setItem('role',res.user.role);
                sessionStorage.setItem('username',res.user.fullName);
                this.router.navigate(['/user'])
              break;
           }
        }
        else{
          swal.fire(res.message)
        }
      }, error =>{
        swal.fire("No hay conexion con el servidor ", "warning")
      })

  }

  removeInfo(){
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('token');
  }
}
