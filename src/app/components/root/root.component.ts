import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config-service';
import { DataService } from 'src/app/services/data.service';
import { TrasladoComponent } from '../traslado/traslado.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {

  public listModules: any
  public dateNow = new Date()
  constructor(private config: ConfigService,
              private router: Router,
              public dataServices: DataService,
              private trasladoComponent: TrasladoComponent) { }

  ngOnInit() {

    this.dataServices.username =  sessionStorage.getItem('username')
    switch (sessionStorage.getItem('role')) {
        case this.config.getConfig().typerol.root:
           this.getListModules()
        break;

        case this.config.getConfig().typerol.bodega:
        case this.config.getConfig().typerol.auxbodega:
          this.getListModulesBD()
        break;
    
      default:
        break;
    }
  }

  getListModules()
  {
    this.listModules = this.config.getConfig().modulos
  }

  getListModulesBD()
  {
    this.listModules = this.config.getConfig().modulosbd
  }

  salir(){
    this.router.navigate(["/"])
  }

  save()
  {
    this.trasladoComponent.save()
  }

}
