import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/services/config-service';
import { PuntoService } from 'src/app/services/punto.service';
import { DataService } from 'src/app/services/data.service';
import swal from 'sweetalert2';
import { ProductService } from 'src/app/services/product.service';
import { UtilServices } from '../util/util';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public formR: FormGroup
  public formU: FormGroup
  public name: AbstractControl
  public ctg: AbstractControl
  public vunit:  AbstractControl
  public cnt:  AbstractControl
  public tipoEmpaque:  AbstractControl
  public cntUnd:  AbstractControl
  public ref: AbstractControl
  public vventa: AbstractControl
  public file: AbstractControl
  public showLoader = false
  public submitted = false
  public showError = false
  public showformU = false
  public sb = true
  public sb2 = false
  public ptvs: any
  public msgError: string
  public opc = 0
  public fileN: any

  public showform = false
  public listCtg: any
  public presenTation: any
  
  constructor(
    private formBuilder: FormBuilder,
    private config: ConfigService,
    private productService: ProductService,
    private categoryService: CategoryService,
    public utilService: UtilServices,
    private dataService: DataService) 
    {
    this.formR = this.formBuilder.group({
      _id: [''],
      name: ['', Validators.required ],
      ctg: ['Niña', Validators.required ],
      vunit: ['', Validators.required ],
      vventa:['', Validators.required ],
      cnt: ['', Validators.required ],
      tipoEmpaque:[1, Validators.required ],
      cntUnd:[''],
      ref:['', Validators.required ]
    })
    this.name = this.formR.controls['name']
    this.ctg = this.formR.controls['ctg']
    this.vunit = this.formR.controls['vunit']
    this.cnt = this.formR.controls['cnt']
    this.tipoEmpaque = this.formR.controls['tipoEmpaque']
    this.cntUnd = this.formR.controls['cntUnd']
    this.ref = this.formR.controls['ref']
    this.vventa = this.formR.controls['vventa']

    this.formU = this.formBuilder.group({
      file: ['', Validators.required ]
    })
    this.file = this.formU.controls['file']
  }

  ngOnInit() {
    this.getProducts()
    this.getCTg()
    this.dataService.title = "Gestión de Productos en Bodega"
    this.presenTation = this.config.getConfig().typepresentation
  }

  getCTg(){
    this.categoryService.get().subscribe( (res:any)=>{
      this.listCtg =  res
    }, error =>{
      swal.fire("Aviso","Ocurrio un error al consultar categorias", "warning")
    })
  }

  buscarProduct(event:any){
   this.productService.seach({ name: event.target.value }).subscribe( (res:any)=>{
      if(res.status == true){
        this.showform = true
        this.ptvs = res.product
        this.sb = false
        this.sb2 = false
      }
   }, error =>{
     this.msgError = "Ocurrio un error al consultar producto"
     this.showError = true
   })
  }
  get f() {
    return this.formR.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  showHide(){
     this.clear() 
     this.showform = !this.showform
     this.sb = true
     this.sb2 = false
  }

  clear(){
    this.formR.reset()
    this.formU.reset()
  }

  getProducts(){
    this.show()
    this.productService.get().subscribe( (res:any) =>{
      this.ptvs = res
      this.sb = true
      this.sb2 = false
      this.formR.reset()
    }, error => {
      swal.fire("Aviso","Ocurrio un error al consultar productos", "warning")
      this.hide()
    }) 
    this.hide()
  }

  saveEdit(){
    if(this.formR.value._id != ""){
      this.show()
      this.formR.value.vunit = this.utilService.replaceAll( this.formR.value.vunit, ".", "" ) 
      this.formR.value.vventa = this.utilService.replaceAll( this.formR.value.vventa, ".", "" ) 
      this.productService.update(this.formR.value).subscribe( (res:any)=>{
        if(res.status == true)
         {
          this.getProducts()
          this.showHide()
          this.formR.reset()
          this.submitted = false
         }
      }, error =>{
         this.hide()
         swal.fire("Aviso",this.config.getMessage().error.message + "al actualizar producto", "warning")
      })
    }
  }

  save(){
    this.submitted = true
    if(this.formR.invalid)
     return false
     
    this.show()
    this.formR.value.vunit = this.utilService.replaceAll( this.formR.value.vunit, ".", "" )
    this.formR.value.vventa = this.utilService.replaceAll( this.formR.value.vventa, ".", "" ) 
    this.productService.save(this.formR.value).subscribe( (res:any)=>{
      if(res.status == true)
      {
        this.getProducts()
        this.showHide()
        this.formR.reset()
        this.submitted = false
      }
    }, error =>{
      this.hide()
      swal.fire("Aviso",this.config.getMessage().error.message + "al registrar punto de venta", "warning")
    })
  }

  onFileSelected(event:any) {
    if(event.target.files.length > 0) 
     {
      //this.formU.get('file').setValue(event.target.files[0])
      
      console.log(event.target.files[0]);
      let data = { file:event.target.files[0] }
      console.log( data );
      // this.productService.uploap(data).subscribe( (res:any)=>{
      //   console.log(res);
      // })
       
     }
   }

  saveFile(){
   this.productService.uploap(this.fileN).subscribe( (res:any)=>{
     console.log(res);
   })
  }

  
  delete(id: string){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Está seguro eliminar punto de venta ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      
       for (let i = 0; i < this.ptvs.length; i++) {
         if(id == this.ptvs[i]._id){
           this.ptvs.splice(i,1)
           this.productService.delete({ _id: id }).subscribe( (res:any)=>{
            if(res.status == true)
               this.getProducts()
          }, error =>{
             swal.fire("Aviso",this.config.getMessage().error.message + "al eliminar punto de venta", "warning")
          })
         }
       }
     }
    })
   
  }

  edit(item: any){
    this.showHide()
    this.sb = false
    this.sb2 = true
    this.formR.get('_id').setValue(item._id)
    this.formR.get('ref').setValue(item.ref)
    this.formR.get('name').setValue(item.name)
    this.formR.get('ctg').setValue(item.ctg)
    this.formR.get('vunit').setValue(item.vunit)
    this.formR.get('cnt').setValue(item.cnt)
    this.formR.get('cntUnd').setValue(item.cntUnd)
    this.formR.get('tipoEmpaque').setValue(item.tipoEmpaque)
    this.formR.get('vventa').setValue(item.vventa)
  }

  validateNumberFormat(event: any, opc?:number) 
  {
    console.log( opc );
    event.target.value = (event.target.value + '').replace(/[^0-9]/g, '')
    if(opc == 1)
     this.formR.get('vunit').setValue(this.utilService.formatNumber(event.target.value, " "))
    if(opc == 2)
     this.formR.get('vventa').setValue(this.utilService.formatNumber(event.target.value, " "))
  }
  
}
