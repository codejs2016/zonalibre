import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/services/config-service';
import { PuntoService } from 'src/app/services/punto.service';
import { DataService } from 'src/app/services/data.service';
import swal from 'sweetalert2';
import { UtilServices } from '../util/util';

@Component({
  selector: 'app-puntos',
  templateUrl: './puntos.component.html',
  styleUrls: ['./puntos.component.css']
})
export class PuntosComponent implements OnInit {

  public formR: FormGroup
  public name: AbstractControl 
  public showLoader = false
  public submitted = false
  public showError = false
  public sb = true
  public sb2 = false
  public ptvs: any
  public msgError: string

  public showform = false
  
  
  constructor(
    private formBuilder: FormBuilder,
    private config: ConfigService,
    private puntoService: PuntoService,
    private dataService: DataService,
    private utilService: UtilServices) 
    {
    this.formR = this.formBuilder.group({
      _id: [''],
      name: ['', Validators.required ],
    })
    this.name = this.formR.controls['name']
  }

  ngOnInit() {
    this.getPtv()
    this.dataService.title = "Gestión de Puntos de Ventas"
  }


  get f() {
    return this.formR.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  showHide(){
    this.showform = !this.showform
    this.sb = true
    this.sb2 = false
  }

  clear(){
    this.formR.reset()
  }

  getPtv(){
    this.show()
    this.puntoService.get().subscribe( (res:any) =>{
      this.ptvs = res
      this.sb = true
      this.sb2 = false
      this.formR.reset()
    }, error => {
      swal.fire("Aviso","Ocurrio un error al consultar puntos de venta", "warning")
      this.hide()
    }) 
    this.hide()
  }

  saveEdit(){
    this.submitted = true
    if(this.formR.invalid)
     return false

    if(this.formR.value._id != ""){
      this.show()
      this.puntoService.update(this.formR.value).subscribe( (res:any)=>{
        if(res.status == true)
         {
          this.getPtv()
          this.showHide()
          this.submitted = false
          this.formR.reset()
         }
      }, error =>{
         this.hide()
         swal.fire("Aviso",this.config.getMessage().error.message + "al actualizar punto de venta", "warning")
      })
    }
  }

  save(){
    this.submitted = true
    if(this.formR.invalid)
     return false
     
    this.show()
    this.puntoService.save(this.formR.value).subscribe( (res:any)=>{
      if(res.status == true)
      {
        this.getPtv()
        this.showHide()
        this.formR.reset()
        this.submitted = false
      }
    }, error =>{
      this.hide()
      swal.fire("Aviso",this.config.getMessage().error.message + "al registrar punto de venta", "warning")
    })
    
  }
  
  delete(id: string){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Está seguro eliminar punto de venta ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      
       for (let i = 0; i < this.ptvs.length; i++) {
         if(id == this.ptvs[i]._id){
           this.ptvs.splice(i,1)
           this.puntoService.delete({ _id: id }).subscribe( (res:any)=>{
            if(res.status == true)
               this.getPtv()
          }, error =>{
             swal.fire("Aviso",this.config.getMessage().error.message + "al eliminar punto de venta", "warning")
          })
         }
       }
     }
    })
   
  }

  edit(item: any){
    this.showHide()
    this.sb = false
    this.sb2 = true
    this.formR.get('_id').setValue(item._id)
    this.formR.get('name').setValue(item.name)
  }

}
