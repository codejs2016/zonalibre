import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/services/config-service';
import { PuntoService } from 'src/app/services/punto.service';
import { DataService } from 'src/app/services/data.service';
import swal from 'sweetalert2';
import { UtilServices } from '../util/util';
import { CategoryService } from 'src/app/services/category.service';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  public formR: FormGroup
  public name: AbstractControl 
  public showLoader = false
  public submitted = false
  public showError = false
  public sb = true
  public sb2 = false
  public ptvs: any
  public msgError: string

  public showform = false
  
  
  constructor(
    private formBuilder: FormBuilder,
    private config: ConfigService,
    private categoryService: CategoryService,
    private dataService: DataService,
    private utilService: UtilServices) 
    {
    this.formR = this.formBuilder.group({
      _id: [''],
      name: ['', Validators.required ],
    })
    this.name = this.formR.controls['name']
  }

  ngOnInit() {
    this.getPtv()
    this.dataService.title = "Gestión de Puntos de Categorias"
  }


  get f() {
    return this.formR.controls;
  }

  show(){
    this.showLoader = true
  }

  hide(){
    this.showLoader = false
  }

  showHide(){
    this.showform = !this.showform
    this.sb = true
    this.sb2 = false
  }

  clear(){
    this.formR.reset()
  }

  getPtv(){
    this.show()
    this.categoryService.get().subscribe( (res:any) =>{
      this.ptvs = res
    }, error => {
      swal.fire("Aviso","Ocurrio un error al consultar categorias", "warning")
      this.hide()
    }) 
    this.hide()
  }

  saveEdit(){
    this.submitted = true
    if(this.formR.invalid)
     return false
     
    if(this.formR.value._id != ""){
      this.show()
      this.categoryService.update(this.formR.value).subscribe( (res:any)=>{
        if(res.status == true)
         {
          this.getPtv()
          this.showHide()
          this.formR.reset()
          this.submitted = false
         }
      }, error =>{
         this.hide()
         swal.fire("Aviso",this.config.getMessage().error.message + "al actualizar punto de categoria", "warning")
      })
    }
  }

  save(){
    this.submitted = true
    if(this.formR.invalid)
     return false
     
    this.show()
    this.categoryService.save(this.formR.value).subscribe( (res:any)=>{
        if(res.status == true)
        {
          this.getPtv()
          this.showHide()
          this.formR.reset()
          this.submitted = false
        }
      }, error =>{
        this.hide()
        swal.fire("Aviso",this.config.getMessage().error.message + "al registrar punto de venta", "warning")
      })
  }
  
  delete(id: string){
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Está seguro eliminar categoria ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
    if (result.value)
    {
      
       for (let i = 0; i < this.ptvs.length; i++) {
         if(id == this.ptvs[i]._id){
           this.ptvs.splice(i,1)
           this.categoryService.delete({ _id: id }).subscribe( (res:any)=>{
            if(res.status == true)
               this.getPtv()
          }, error =>{
             swal.fire("Aviso",this.config.getMessage().error.message + "al eliminar categoria", "warning")
          })
         }
       }
     }
    })
   
  }

  edit(item: any){
    this.showHide()
    this.sb = false
    this.sb2 = true
    this.formR.get('_id').setValue(item._id)
    this.formR.get('name').setValue(item.name)
  }


}
